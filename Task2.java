/* Необходимо написать программу, где бы пользователю предлагалось ввести число на выбор:
       1, 2 или 3, а программа должна сказать, какое число ввёл пользователь: 1, 2, или 3.
       Написать двумя способами. If и switch.
       */
import java.util.Scanner;
public class Task2 {
    public static void main(String[] args) {
        System.out.println("Введите число 1, 2 или 3:" + "\n");
        Scanner inputFigure = new Scanner(System.in);
        int i = inputFigure.nextInt();
        String b = "You wrote integer " +i;
        String h = "No write the correct integer";
        switch (i) {
            case 1:
                System.out.println(b);
                break;
            case 2:
                System.out.println(b);
                break;
            case 3:
                System.out.println(b);
                break;
            default:
                System.out.println(h);
                break;
        }
    }
}

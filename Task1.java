/* Необходимо написать программу, где бы пользователю предлагалось ввести число на выбор:
       1, 2 или 3, а программа должна сказать, какое число ввёл пользователь: 1, 2, или 3.
       Написать двумя способами. If и switch.
       */
import java.util.Scanner;
public class Task1 {
    public static void main(String[] args) {
        System.out.println("Введите число 1, 2 или 3:" + "\n");
        Scanner inputFigure = new Scanner(System.in);
        int i = inputFigure.nextInt();
        if (i == 1) {
            System.out.println("You wrote integer 1");
        } else if (i == 2) {
            System.out.println("You wrote integer 2");

        } else if (i == 3) {
            System.out.println("You wrote integer 3");

        } else {
            System.out.println("You wrote wrong integer");
        }

    }
}